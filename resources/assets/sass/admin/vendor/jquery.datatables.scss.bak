table {
  &.dataTable {
    &.no-footer {
      border-bottom: 1px solid $border-color;
      margin-bottom: 20px;
    }
  }
}

.sorting_asc {
  &:focus {
    outline: none;
  }
}

.dataTables_wrapper {
  overflow: hidden;
  padding-bottom: 5px;

  .dataTables_length{
    color: $default-dark;
    float: left;

    @include to($breakpoint-sm) {
      text-align: left;
    }

    select {
      border: 1px solid $border-color;
      border-radius: 2px;
      box-shadow: none;
      height: 35px;
      font-size: 14px;
      padding: 5px;
      margin-left: 5px;
      margin-right: 5px;
      color: $default-text-color;
      transition: all 0.2s ease-in;
    }
  }

  .dataTables_filter {
    color: $default-dark;
    float: right;

    @include to($breakpoint-sm) {
      text-align: left;
    }

    input {
      border: 1px solid $border-color;
      border-radius: 2px;
      box-shadow: none;
      height: 35px;
      font-size: 14px;
      margin-left: 15px;
      padding: 5px;
      color: $default-text-color;
      transition: all 0.2s ease-in;
    }
  }

  .dataTables_info {
    color: $default-text-color;
    float: left;
  }

  .dataTables_processing {
    color: $default-dark;
  }

  .dataTables_paginate {
    color: $default-text-color;
    float: right;

    .paginate_button {
      color: $default-text-color !important;
      padding: 6px 12px;
      border-radius: 2px;
      margin-right: 10px;
      transition: all 0.2s ease-in-out;
      text-decoration: none;

      &.next,
      &.previous,
      &.first,
      &.last {
        border-radius: 2px;
        text-decoration: none;

        &:hover,
        &:focus {
          color: #fff !important;
        }

        &.disabled {
          opacity: 0.4;
          pointer-events: none;
        }
      }

      &:hover {
        color: #fff !important;
        background: $default-primary;
      }

      &.current {
        color: #fff !important;
        background: $default-primary;

        &:hover {
          color: $default-white !important;
          background: $default-primary;
        }
      }
    }
  }

  .status {
    width: 5px;
    height: 5px;
  }
}

table.dataTable thead>tr>th.sorting_asc,
table.dataTable thead>tr>th.sorting_desc,
table.dataTable thead>tr>th.sorting,
table.dataTable thead>tr>td.sorting_asc,
table.dataTable thead>tr>td.sorting_desc,
table.dataTable thead>tr>td.sorting {
    padding-right: 30px;
}

table.dataTable thead>tr>th:active,
table.dataTable thead>tr>td:active {
    outline: none;
}

table.dataTable thead .sorting,
table.dataTable thead .sorting_asc,
table.dataTable thead .sorting_desc,
table.dataTable thead .sorting_asc_disabled,
table.dataTable thead .sorting_desc_disabled {
    cursor: pointer;
    position: relative;
}

table.dataTable thead .sorting:before,
table.dataTable thead .sorting:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before,
table.dataTable thead .sorting_desc_disabled:after {
    position: absolute;
    bottom: 0.9em;
    display: block;
    opacity: 0.3;
}

table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc_disabled:before {
    right: 1em;
    content: "\2191";
}

table.dataTable thead .sorting:after,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_desc_disabled:after {
    right: 0.5em;
    content: "\2193";
}

table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_desc:after {
    opacity: 1;
}

table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc_disabled:after {
    opacity: 0;
}

div.dataTables_scrollHead table.dataTable {
    margin-bottom: 0 !important;
}

div.dataTables_scrollBody table {
    border-top: none;
    margin-top: 0 !important;
    margin-bottom: 0 !important;
}

div.dataTables_scrollBody table thead .sorting:before,
div.dataTables_scrollBody table thead .sorting_asc:before,
div.dataTables_scrollBody table thead .sorting_desc:before,
div.dataTables_scrollBody table thead .sorting:after,
div.dataTables_scrollBody table thead .sorting_asc:after,
div.dataTables_scrollBody table thead .sorting_desc:after {
    display: none;
}

div.dataTables_scrollBody table tbody tr:first-child th,
div.dataTables_scrollBody table tbody tr:first-child td {
    border-top: none;
}

div.dataTables_scrollFoot>.dataTables_scrollFootInner {
    box-sizing: content-box;
}

div.dataTables_scrollFoot>.dataTables_scrollFootInner>table {
    margin-top: 0 !important;
    border-top: none;
}

@media screen and (max-width: 767px) {
    div.dataTables_wrapper div.dataTables_length,
    div.dataTables_wrapper div.dataTables_filter,
    div.dataTables_wrapper div.dataTables_info,
    div.dataTables_wrapper div.dataTables_paginate {
        text-align: center;
    }
}

table.dataTable.table-sm>thead>tr>th {
    padding-right: 20px;
}

table.dataTable.table-sm .sorting:before,
table.dataTable.table-sm .sorting_asc:before,
table.dataTable.table-sm .sorting_desc:before {
    top: 5px;
    right: 0.85em;
}

table.dataTable.table-sm .sorting:after,
table.dataTable.table-sm .sorting_asc:after,
table.dataTable.table-sm .sorting_desc:after {
    top: 5px;
}
