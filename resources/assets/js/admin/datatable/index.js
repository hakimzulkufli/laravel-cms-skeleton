import * as $ from 'jquery';
// import 'datatables.net';
import 'datatables.net-bs4';
// import 'datatables.net-select';
import 'datatables.net-select-bs4';

export default (function () {
  $('#dataTable').DataTable();

  $('#dataTable.dT-select').DataTable({
        select: {
            style: 'multi'
        }
    });
}());
