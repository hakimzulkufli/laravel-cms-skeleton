@extends('admin.layouts.default')
@section('page-title', 'Add User')
@section('content')
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <h5 class="c-grey-900">User Details</h5>
        <div class="mT-30">
            {!! Form::model($user, ['route' => 'admin.users.store']) !!}

                <div class="row">
                    <div class="form-group col">
                        {!! Form::label('name', 'Full Name') !!}
                        {!! Form::text('name', '', ['class' => 'form-control', 'required']) !!}
                    </div>

                    <div class="form-group col">
                        {!! Form::label('email', 'Email Address') !!}
                        {!! Form::email('email', '', ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        {!! Form::label('password', 'Password') !!}
                        {!! Form::password('password', ['class' => 'form-control', 'aria-describedby' => 'password-help', 'required']) !!}
                        {!! Html::tag('small', 'Password must be at least 6 characters long.', ['class' => 'form-text text-muted', 'id' => 'password-help']) !!}
                    </div>

                    <div class="form-group col">
                        {!! Form::label('password_confirmation', 'Confirm Password') !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        {!! Form::label('roles', 'Roles') !!}
                    </div>
                </div>

                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection
