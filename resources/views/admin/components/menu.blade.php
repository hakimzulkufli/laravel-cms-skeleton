<li class="nav-item mT-30 active">
    <a class="sidebar-link" href="{{ route('admin.dashboard') }}">
    <span class="icon-holder">
        <i class="c-blue-500 ti-home"></i>
    </span>
    <span class="title">Dashboard</span>
    </a>
</li>
<li class="nav-item">
    <a class='sidebar-link' href="{{ route('admin.users.index') }}">
    <span class="icon-holder">
        <i class="c-brown-500 ti-user"></i>
    </span>
    <span class="title">Users</span>
    </a>
</li>
<li class="nav-item">
    <a class='sidebar-link' href="{{ route('admin.groups.index') }}">
    <span class="icon-holder">
        <i class="c-yellow-500 ti-layers-alt"></i>
    </span>
    <span class="title">Groups</span>
    </a>
</li>

