@extends('admin.layouts.default')
@section('page-title', 'Groups')
@section('content')
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <a href="{{ route('admin.groups.create') }}" class="btn btn-info">Add Group</a>
    </div>

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($groups as $group)
                    <tr>
                        <td>
                            {{ $group->name }}
                        </td>
                        <td>
                            {{ $group->description }}
                        </td>
                        <td>
                            <a href="{{ route('admin.groups.show', $group->slug) }}" class="btn btn-primary btn-sm btn-block">View</a>
                        </td>
                        <td>
                            {!! Form::model($group, ['route' => ['admin.groups.destroy', $group->uuid_text], 'method' => 'DELETE']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm btn-block']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
