@extends('admin.layouts.default')
@section('page-title', 'Add Member')
@section('content')
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <div class="row">
            <div class="col-md-2">
                <h5 class="fw-600 c-grey-900">Group Name</h5>
                <p>{{ $group->name }}</p>
            </div>
            <div class="col-md-10">
                <h5 class="fw-600 c-grey-900">Description</h5>
                <p>{{ $group->description }}</p>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <h5 class="fw-600 c-grey-900 mB-10">Users</h5>
                <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{ $user->name }}
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{-- <a href="{{ route('admin.groups.remove_member', [$group, $member]) }}" class="btn btn-sm btn-danger"></a> --}}
                                    {!! Form::model($group, ['route' => ['admin.groups.members.store', $group->slug, $user->slug], 'method' => 'POST']) !!}
                                        {!! Form::submit('Add To Group', ['class' => 'btn btn-success btn-sm btn-block']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
