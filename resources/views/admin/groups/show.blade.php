@extends('admin.layouts.default')
@section('page-title', 'View Group')
@section('content')
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <div class="row no-gutters">
            <div class="col-md-1" style="margin-right: 1rem">
                <a href="{{ route('admin.groups.members.add', $group) }}" class="btn btn-primary btn-sm btn-block">Add New Member</a>
            </div>
            <div class="col-md-1" style="margin-right: 1rem">
                <a href="{{ route('admin.groups.edit', $group) }}" class="btn btn-info btn-sm btn-block">Edit Group</a>
            </div>
            <div class="col-md-1">
                {!! Form::model($group, ['route' => ['admin.groups.destroy', $group->uuid_text], 'method' => 'DELETE']) !!}
                    {!! Form::submit('Delete Group', ['class' => 'btn btn-danger btn-sm btn-block']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <div class="row">
            <div class="col-md-2">
                <h5 class="fw-600 c-grey-900">Group Name</h5>
                <p>{{ $group->name }}</p>
            </div>
            <div class="col-md-10">
                <h5 class="fw-600 c-grey-900">Description</h5>
                <p>{{ $group->description }}</p>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <h5 class="fw-600 c-grey-900 mB-10">Members</h5>
                <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date Joined</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($group->members as $member)
                            <tr>
                                <td>
                                    {{ $member->name }}
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::parse($member->pivot->created_at)->toDayDateTimeString() }}
                                </td>
                                <td>
                                    {{-- <a href="{{ route('admin.groups.remove_member', [$group, $member]) }}" class="btn btn-sm btn-danger"></a> --}}
                                    {!! Form::model($group, ['route' => ['admin.groups.members.remove', $group->slug, $member->slug], 'method' => 'POST']) !!}
                                        {!! Form::submit('Remove', ['class' => 'btn btn-danger btn-sm btn-block']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <h5 class="fw-600 c-grey-900 mB-10">Add New Member</h5>
                <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{ $user->name }}
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{-- <a href="{{ route('admin.groups.remove_member', [$group, $member]) }}" class="btn btn-sm btn-danger"></a> --}}
                                    {!! Form::model($group, ['route' => ['admin.groups.members.store', $group->slug, $user->slug], 'method' => 'POST']) !!}
                                        {!! Form::submit('Add To Group', ['class' => 'btn btn-success btn-sm btn-block']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
