@extends('admin.layouts.default')
@section('page-title', 'Add Group')
@section('content')
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        {{-- <h5 class="c-grey-900">Group Details</h5> --}}
        <div class="">
            {!! Form::model($group, ['route' => 'admin.groups.store']) !!}

                <div class="form-group row">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('name', '', ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('description', '', ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection
