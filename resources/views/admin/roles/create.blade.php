@extends('admin.layouts.default')
@section('page-title', 'Add Role')
@section('content')
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        {{-- <h5 class="c-grey-900">Group Details</h5> --}}
        <div class="">
            {!! Form::model($role, ['route' => 'admin.roles.store']) !!}

                <div class="form-group row">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('name', '', ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    {!! Form::label('permissions', 'Assign Permission(s)', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        @foreach($permissions as $permission)
                            {{ Form::checkbox('permissions[]', $permission->id) }}
                            {{ Form::label($permission->name, ucfirst($permission->name)) }}
                        @endforeach
                    </div>
                </div>

                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection
