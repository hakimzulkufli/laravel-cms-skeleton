@extends('admin.layouts.default')
@section('page-title', 'Roles')
@section('content')
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <a href="{{ route('admin.roles.create') }}" class="btn btn-info">Add Role</a>
    </div>

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead class="thead-dark">
                <tr>
                    <th>Role</th>
                    <th>Permissions</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tfoot class="thead-dark">
                <tr>
                    <th>Role</th>
                    <th>Permissions</th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
            <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>
                            {{ $role->name }}
                        </td>
                        <td>
                            {{ $role->permissions->name }}
                        </td>
                        <td>
                            <a href="{{ route('admin.roles.edit', $group->slug) }}" class="btn btn-primary btn-sm btn-block">Edit</a>
                        </td>
                        <td>
                            {!! Form::model($role, ['route' => ['admin.roles.destroy', $role->slug], 'method' => 'DELETE']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm btn-block']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
