<?php

use App\User;
use App\Group;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\User::class, 1000)->create();

        $user = User::create([
            'name' => 'Administrator',
            'email' => 'admin@mail.com',
            'password' => Hash::make('123456')
        ]);

        $group = Group::where('slug', 'human-resources')->firstOrFail();
        $user->groups()->attach($group->id);
    }
}
