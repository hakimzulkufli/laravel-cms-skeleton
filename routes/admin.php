<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin panel routes for your application.
| These routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('admin.dashboard.index'); })->name('dashboard');
Route::resource('users', 'UserController');

/*
 *  Group routes
 */
Route::resource('groups', 'GroupController');
Route::get('/groups/{group}/add', 'GroupController@addMember')->name('groups.members.add');
Route::post('/groups/{group}/add/{user}', 'GroupController@storeMember')->name('groups.members.store');
Route::post('/groups/{group}/remove/{user}', 'GroupController@removeMember')->name('groups.members.remove');

Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');
