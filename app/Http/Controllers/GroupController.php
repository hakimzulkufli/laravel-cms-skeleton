<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        return view('admin.groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = new Group;
        return view('admin.groups.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255'
        ]);

        $group = Group::create($data);

        return redirect()->route('admin.groups.index')->with('success', 'Group "'.$group->name.'" successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        return view('admin.groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('admin.groups.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $data = $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255'
        ]);

        $group->fill($data)->save();

        return redirect()->route('admin.groups.index')->with('success', 'Group "'.$group->name.'" successfully updated.');
    }

    public function addMember(Group $group)
    {
        $users = User::all();
        return view('admin.groups.add_member', compact('group', 'users'));
    }

    public function storeMember(Group $group, User $user)
    {
        $user->groups()->attach($group->id);
        return redirect()->route('admin.groups.show', $group->slug)->with('success', 'User "'.$user->name.'" added to the group.');
    }

    public function removeMember(Group $group, User $user)
    {
        $user->groups()->detach($group->id);
        return redirect()->back()->with('success', 'User "'.$user->name.'" removed from the group.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $group->delete();
        return redirect()->route('admin.groups.index')->with('success', 'Group successfully deleted.');
    }
}
