<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use Illuminate\Http\Request;
use App\Transformers\Json;
use Tymon\JWTAuth\Exceptions\JWTException;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Mail;
use App\User;
class AuthController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function register(Request $request){
        $data = $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        return response()->json(Json::response('OK', $user, 'User Have been Registered, please login to continue'));
    }

    public function login(Request $request){
        $credentials = $request->only(['email', 'password']);
        $token = null;

        try{
            if(!$token = auth()->attempt($credentials)) {
                return response()->json([
                    'error' => 'Unauthorized, invalid email or password',
                ], 401);
            }
        } catch (Exception $e){
            return response()->json([
                'message' => 'Failed to generate token.',
            ], 500);
        }

        $user = auth()->user();

        return response()->json(Json::response('OK', $user, null, ['token' => $token]));
    }
}
